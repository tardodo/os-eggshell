
//Variable to update terminal name
char termName[32];

#define MAX_ARGS 32
#define TERM_NAME "eggsy"
#define MAXS 64

//Sets and initializes variables
void setEnv();

//Separates inputted data into arguments
int parseInp(char *input, char *args[]);

//Resolves any special characters in arguments
bool resolveArgs(char *args[], int tokenIndex);

//Checks for and does substitutions
bool SubsCheck(char *str, char *v);

//Checks for variable assignment
bool isVariable(char *args[], int tokenIndex);

//Method with all internal commands
bool Commands(char *args[], int tokenIndex);

//Runs external commands and piping
void ExternalCom(char *args[], int tokenIndex);

//Redirects output to a file
bool redirOut(char *args[], int tokenIndex, int stdOut);

//Restores output to terminal
void restoreOut(int stdOut);

//Redirect input to read from file
bool redirInput(char *args[], int tokenIndex, int stdIn);

//Function used for piping
bool piping(char *args[], int tokenIndex);

//Signal handler for SIGCHLD
void sigChildHandler(int signum);

//General sigHandler used for SIGINT and SIGTSTP
void sigHandler(int signum);

//Checks if a process should be run in the background
bool runBackground(char *args[], int tokenIndex);