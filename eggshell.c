#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "linenoise/linenoise.h"

#include "functions.h"

extern char **environ;

int main(void)
{
    char *line;
    char *args[MAX_ARGS];
    int tokenIndex;
    char temp[1024];

    //Initialise environment and set variables
    setEnv();
    
    //begin reading input
    while ((line = linenoise(termName)) != NULL)
    {   
        //make a copy of data input
        strcpy(temp, line);

        //parse data input into several arguments
        tokenIndex = parseInp(temp, args);

        //if no data is entered, skip everything
        if (tokenIndex > 0)
        {
            //resolve any special characters
            if (resolveArgs(args, tokenIndex))
            {
                //Check if argument is internal command
                if (!Commands(args, tokenIndex))
                {
                    //Check if argument is a variable assignment
                    if(!isVariable(args, tokenIndex))
                    {
                        //Check if argument is external command
                        ExternalCom(args, tokenIndex);
                    }  
                }
            }
        }
        // Free allocated memory
        linenoiseFree(line);
    }
    return 0;
}
