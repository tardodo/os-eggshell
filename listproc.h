
#define NSIZE 45 

//Struct definition for a node
typedef struct process {

    char name[NSIZE];

    pid_t pid;

    struct process * next;

}listProc;

//Traverse list and print each node
listProc *traverse(listProc *head);

//Add a node to the list
void add(listProc *head, pid_t pid, char *name);

//Initialize list to head node
listProc *initializeList(char *name, pid_t pid);

//Deletes a node if pid matches
listProc *deletePid(listProc **head_ref, pid_t key);

//Resume last process suspended
bool resumeLast(listProc *head);

//clear list of inactive pids
void filterList(listProc *head);

//Kill all active processes aside from the head
void killAll(listProc *head);

//Resume specific pid
void resumePid(listProc *head, pid_t pid);