CC=gcc
CFLAGS=-I.
DEPS = functions.h listproc.h linenoise/linenoise.h
OBJ = eggshell.o functions.o listproc.o linenoise/linenoise.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

egg: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)