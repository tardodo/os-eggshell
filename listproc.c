#include <stdio.h>

#include <stdlib.h>  

#include <stdbool.h>

#include <string.h>  

#include <signal.h>

#include <sys/wait.h>

#include "listproc.h"

//Resume last process suspended
bool resumeLast(listProc *head){
    listProc * prev, * current;

    filterList(head);

    current = head;
    
    //go to end of list
    while(current != NULL){
        prev = current;
        current = current-> next;
    }

    //If only one node is in list, return false since
    //the head is taken up by eggshell pid.
    if(prev->pid == head->pid){
        printf("No suspended processes\n");
        return false;
    }else{
        if(kill(prev->pid, SIGCONT) == -1){
            perror("Could not unsuspend process\n");
            return false;
        }
    }
        
    return true;
}

//Referenced from: https://www.geeksforgeeks.org/linked-list-set-3-deleting-node/
//Deletes a node if pid matches
listProc *deletePid(listProc **head_ref, pid_t pid) 
{ 
    // Store head node 
    listProc* temp = *head_ref, *prev; 
  
    //Locate pid
    while (temp != NULL && temp->pid != pid) 
    { 
        prev = temp; 
        temp = temp->next; 
    } 
  
    // If pid not found
    if (temp == NULL){
        printf("Could not find PID\n");
        return NULL;
    } 
     
  
    // Unlink the node from linked list 
    prev->next = temp->next; 
  
    free(temp); 

    //Return next node
    return prev->next;
} 

//Traverse list and print each node
listProc *traverse(listProc *head) {

    listProc *current;
    
    if (head == NULL){
        printf("No data entered. ");
        return NULL;
    }
    else
        printf("Process List:\n");

    filterList(head);
    current = head;

    //Go through list
    while (current != NULL) {
        printf("Name: %s  pid: %d\n", current->name, current->pid);

        //assign next node to current
        current = current->next;
    }
    return current;
}

//Add a node to the list
void add(listProc *head, pid_t pid, char *name){

    listProc * prev, * current;

    current = head;

    //Traverse to end of list
    while(current != NULL){
        prev = current;
        current = current-> next;
    }

    //Create new node
    if(current == NULL){
        current = (listProc *) malloc(sizeof(listProc));
        prev->next = current;
        current->next = NULL;
        strcpy(current->name, name);
        current->pid = pid;
    }
}

//Initialize list to head node
listProc *initializeList(char *name, pid_t pid){
    listProc *head = NULL;

    head = (listProc *) malloc(sizeof(listProc));
    if(head == NULL){
        printf("Out of memory\n");
        return NULL;
    }

    strcpy(head->name, name);
    head->pid = pid;
    head->next = NULL;

    return head;
}

//clear list of inactive pids
void filterList(listProc *head){
    listProc *current;
    current = head;

    while(current != NULL){
        if(kill(current->pid, 0) != 0){
            
            //assign next node to next node of current
            current->next = deletePid(&head, current->pid);
        }
        current = current->next;
    }
}

void killAll(listProc *head){
    listProc *current;

    filterList(head);

    current = head->next;

    while(current != NULL){
        if(kill(current->pid, SIGKILL) == -1){
            perror("Process termination failed");
            break;
        }
        current = current->next;
    }

    filterList(head);

}

void resumePid(listProc *head, pid_t pid){
    listProc *current;

    filterList(head);

    current = head;

    while(current != NULL){
        if(current->pid == pid){
            if(kill(current->pid, SIGCONT) == -1){
                perror("Could not unsuspend process\n");
                break;
                //return false;
            }else{
                break;
            }  
        }
        current = current->next;
    } 
}