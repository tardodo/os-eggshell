#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <limits.h>
#include <ctype.h>
#include <fcntl.h>

//Methods for handling process list
#include "listproc.h"

#include "linenoise/linenoise.h"

#include "functions.h"

//initialise global variables
extern char **environ;
extern const char * const sys_siglist[];

char **shellVars;
int varAmount = 8;
listProc *lP;

sig_t oldSigChldHandler = NULL;
sig_t handlerStp;
sig_t handlerInt;

//Signal handler for SIGCHLD
void sigChildHandler(int signum){
    
    //loop to collect processes
    while(waitpid(-1, NULL, WNOHANG | WUNTRACED)>0){
    };
}

//General signal handler used for SIGINT and SIGTSTP
void sigHandler(int signum){

    //output a newline when caught
    char *newLine = "\n";
    write(STDOUT_FILENO, newLine, strnlen(newLine, sizeof(newLine)));
}

//Function for contextual substitution
//returns true on success, false on failure
//This functions takes a whole string, beginning from $ until the end of the string.
//The function isolates the word to be substituted, and rearranges the string to fit the replacement.
//The new string is placed in the char array fv(full value).
bool SubsCheck(char *str, char fv[1024]){
    
    char copy[1024];
    char value[1024];
    char res[64];
    int cnt;
    int len = (int) strlen(str);

    //make a copy of the string to substitute
    strcpy(copy, str);

    //check if string is valid
    if(str[0] == '$')
    {

        //isolate word to be substituted
        cnt = 1;
        while(isalpha(str[cnt]) != 0){
            res[cnt-1] = copy[cnt];
            cnt++;
        }
        res[cnt-1] = '\0';
        
        //remove any newlines(from file reading)
        if(str[len-1] == '\n'){
            str[len-1] = '\0';
        }
        
        //Locate the matching variable from our custom list of manageing shell variables (shellVars)
        for (int i = 0; i < varAmount; ++i)
        {
            if (strcmp(res, shellVars[i]) == 0)
            {
                strcpy(value, getenv(res));
                
                //add on the rest of the original string
                strcat(value, copy+cnt);

                //copy the final string into the variable fv
                strcpy(fv, value);
                
                
                return true;
            }
        }

        //if no match found, return false
        printf("Variable %s not found\n", res);
        return false;

    //if inputted string isn't correct, return false
    }else{
        
        return false;
    }

}

//Function containing all internal commands
//If the first argument is an internal command, execute and return true
//else return false
bool Commands(char *args[], int tokenIndex){
    int stdOut;

    //Validation check
    if(tokenIndex>0){
        if(strcmp(args[0], "exit") == 0){
            killAll(lP);
            free(lP);
            free(shellVars);
            exit(EXIT_SUCCESS);
        }else if(strcmp(args[0], "echo") == 0){
            int i = 1;

            //check for redirection output
            redirOut(args, tokenIndex, stdOut);

            while(args[i] != NULL && i <tokenIndex){
                printf("%s ", args[i]);
                ++i;
            }
            printf("\n");

            //if output was redirected, restore it back to terminal
            if(stdOut != -1)
                restoreOut(stdOut);
            
            return true;
            
        }else if(strcmp(args[0], "cd") == 0){
            char path[64];

            //error checking for chdir
            if(chdir(args[1]) == -1){
                snprintf(path, sizeof(args[1])+64, "Invalid Path %s", args[1]);
                perror(path); 
            }else{

                //set new CWD on success
                setenv("CWD", getcwd(path, 64), 1);
            }
            return true;

        }else if(strcmp(args[0], "unset") == 0){
            char buf[128];
            int varPos = -1;
            int len = (int) strlen(args[1]);

            //remove newline if reading from file
            if(args[1][len-1] == '\n'){
                args[1][len-1] = '\0';
            }

            //Unset variable and locate within shellVars and remove
            if(unsetenv(args[1]) == -1){
                snprintf(buf, 128, "Could not unset %s", args[1]);
                perror(buf);
            }else{
                for(int i = 0;i<varAmount;++i){
                    if(strcmp(args[1], shellVars[i]) == 0){
                        varPos = i;
                    }
                }
                if(varPos != -1){
                for(int i = varPos; i<varAmount-1;++i){
                    strcpy(shellVars[i], shellVars[i+1]);
                }
                free(shellVars[varAmount-1]);
                varAmount--;
                }else{
                    printf("Could not find in list\n");
                }
            }
            return true;

        }else if(strcmp(args[0], "showenv") == 0){

            //print out all environmental variables
            for(char **env = environ;*env != NULL; env++){
                printf("%s \n", *env);
            }
            return true;

        }else if(strcmp(args[0], "source") == 0){
            FILE *fp;
            char buffer[1024];
            char *res[MAX_ARGS];
            int tIndex;
            int j = 0;   
            
            fp = fopen(args[1], "rb");
            if(fp == NULL){
                printf("Name not found\n");
            }
            else
            {
                //follow same process as eggshell.c
                while (fgets(buffer, 1024, fp) != NULL)
                {
                    tIndex = parseInp(buffer, res);
                    res[tIndex] = NULL;
                    if (tIndex > 0)
                    {
                        if (resolveArgs(res, tIndex))
                        {
                            if (!Commands(res, tIndex))
                            {
                                if (!isVariable(res, tIndex))
                                {
                                    ExternalCom(res, tIndex);
                                }
                            }
                        }
                    }
                    j++;
                }
                fclose(fp);
            }

            return true;

        }else if(strcmp(args[0], "listproc") == 0){

            //function to output all active processes
            traverse(lP);

            return true;

        }else if(strcmp(args[0], "bg") == 0){
            pid_t pid;
            int temp;
            if(args[1] == NULL){
                //Function to resume last suspended process
                resumeLast(lP);
            }else{
                temp = atoi(args[1]);
                pid = (pid_t) temp;
                resumePid(lP, pid);
            }

            return true;
        }
        //if no match, return false
        else {
            return false;
            
        }
    }else
    {
        return false;
    }
    
}

//Split data input into arguments
//Parsing of double quotes also included here
int parseInp(char input[1024], char *args[]){
    char *token = NULL;
    char temp[1024];
    char *quotesPointer;
    char *slashPointer;
    char *endQuotes;
    int endQuotesPos;
    int quotesPos;
    int slashPos;
    int tokenIndex;
    bool quotes = false;
    bool findEnd = false;
    char *findEndQuotes;
    int numQuotes = 0;

    //make a copy of input
    strcpy(temp, input);

    //tokenize copy
    token = strtok(temp, " ");

    //find double quotes in original input
    quotesPointer = memchr(input, '"', strlen(input));

    //Finding all quotes
    while(quotesPointer){
        if((quotesPointer-1)[0] != '\\'){
            numQuotes++;
            if(numQuotes == 2){
                endQuotes = quotesPointer;
            }
        }
        quotesPointer = memchr(quotesPointer+1, '"', strlen(quotesPointer+1));
    }

    //Checking if quote syntax is correct
    if(numQuotes%2 != 0){
        printf("Mismatched quotes\n");
        return 0;
    }else if(numQuotes > 2){
        printf("Too many quotes \nOnly one pair necessary\n");
        return 0;
    }else{
        quotes = true;
    }
    
    //reset quotesPointer to original position
    quotesPointer = memchr(input, '"', strlen(input));

    //give positions of found characters
    quotesPos = (int)(quotesPointer-input+1);
    endQuotesPos = (int)(endQuotes - input +1);

    //go through tokens
    for (tokenIndex = 0; token != NULL && tokenIndex < MAX_ARGS - 1; tokenIndex++)
    {
        //if double quotes are found in a token, the string encapsulated in the quotes is placed as an argument.
        //Now the tokenIndex remains unmoved until the end of the double quotes is found.
        //Tokenization proceeds as usual.
        if (token[0] == '"' && quotes)
        {
            quotesPointer[endQuotesPos - quotesPos] = '\0';
            quotesPointer[0] = '\0';
            args[tokenIndex] = quotesPointer + 1;
            findEnd = true;
        }
        else if (findEnd)
        {
            findEndQuotes = memrchr(token, '"', strlen(token));
            slashPointer = memrchr(token, '\\', strlen(token));
            slashPos = (int)(slashPointer - token + 1);
            endQuotesPos = (int)(findEndQuotes - token + 1);
            if (findEndQuotes && (endQuotesPos - slashPos) != 1)
            {
                findEnd = false;
            }
            tokenIndex--;
        }
        else
            args[tokenIndex] = token;
        
        token = strtok(NULL, " ");
    }

    // set last argument to NULL
    args[tokenIndex] = NULL;
    
    //return number of arguments
    return tokenIndex;
}

//Initialise and set environment
void setEnv(){
    char cwd[128];
    char shell[64];
    char *envName[] = {"PATH", "PROMPT", "CWD", "USER", "HOME", "SHELL", "TERMINAL", "EXITCODE"};
    int valid = 0;

    //shellVars will contain all managing shell vars, as well as new ones defined
    shellVars = (char **)malloc(varAmount * sizeof(char *));

    getcwd(cwd,sizeof(cwd));

    //populate shellVars
    for (int i = 0; i < varAmount; ++i)
    {
        shellVars[i] = (char *)malloc(MAXS * sizeof(char));
        strcpy(shellVars[i], envName[i]);
    }

    //get shell path
    valid = readlink("/proc/self/exe", shell, 64);
    if(valid < 0){
        perror("Shell");
        strcpy(shell, "Couldn't define shell");
    }else{
        shell[valid] = '\0';
    }

    //set initial terminal name
    strcpy(termName, TERM_NAME);
    strcat(termName, "> ");

    //set environment variables
    setenv("PROMPT", TERM_NAME, 1);
    setenv("CWD", cwd, 1);
    setenv("SHELL", shell, 1);
    setenv("TERMINAL", ttyname(STDIN_FILENO), 1);
    setenv("EXITCODE", "No program has been run", 1);

    //initialise listproc with eggshell pid
    lP = initializeList("eggshell", getpid());
    
    //set signal handlers
    handlerStp = signal(SIGTSTP, sigHandler);
    handlerInt = signal(SIGINT, sigHandler);
    signal(SIGCHLD, sigChildHandler);
}

//Checks if user is assigning an environment/shell variable
//Given the nature of VARIABLE=VALUE, a check for '=' is done in the first argument.
bool isVariable(char *args[], int tokenIndex){
    int charact = 0;
    bool newVar = true;

    //validation check
    if (tokenIndex < 2 && tokenIndex>0)
        {
            char value[64];
            char v[1024];
            char var[MAXS];
            
            //locate equals
            while (args[0][charact] != '=' && args[0][charact] != '\0')
            {
                charact++;
            }
            if (args[0][charact] == '=')
            {
                //isolate variable
                for (int i = 0; i < charact; ++i)
                {
                    var[i] = args[0][i];
                }

                var[charact] = '\0';
                charact++;

                
                //check for substitution in value part of argument
                if(SubsCheck(args[0] + charact, v) == true){
                    strcpy(value, v);
                }else{
                    strcpy(value, args[0]+charact);
                }
                    

                //Check if variable is new
                for (int i = 0; i < varAmount; ++i)
                {
                    if (strcmp(var, shellVars[i]) == 0)
                    {
                        newVar = false;
                        setenv(var, value, 1);
                        
                        //If variable is PROMPT, assign new terminal name
                        if(strcmp(var, "PROMPT") == 0){
                            strcpy(termName, getenv("PROMPT"));
                            strcat(termName, "> ");
                        }
                        return true;
                    }
                }  
                
                //If new variable, set and realloc shellVars
                if (newVar)
                {
                    char **tempVars = realloc(shellVars, (varAmount + 1) * sizeof(char *));

                    if (tempVars != NULL)
                    {
                        varAmount++;
                        shellVars = tempVars;
                    }
                    else
                    {
                        puts("Out of memory");
                        exit(EXIT_FAILURE);
                    }

                    shellVars[varAmount - 1] = (char *)malloc(MAXS * sizeof(char));
                    strcpy(shellVars[varAmount - 1], var);
                    setenv(shellVars[varAmount - 1], value, 1);

                    return true;
                }
            }
        }
      return false;
}

//Launch external commands and piping
void ExternalCom(char *args[], int tokenIndex)
{
    int stdOut;
    bool background;
    int stdIn;

    //check if process should be run in background
    background = runBackground(args, tokenIndex);

    //decrement tokenIndex due to '&' removal in runBackground
    if (background)
    {
        tokenIndex--;
    }

    //If piping isn't present, fork and execute single process, else do piping.
    if (!piping(args, tokenIndex))
    {

        pid_t pid_result = fork();

        if (pid_result == -1)
        {
            perror("fork failed");
            exit(EXIT_FAILURE);
        }

        //add process to listproc
        add(lP, pid_result, args[0]);

        //child
        if (pid_result == 0)
        {

            //set child to ignore SIGCHLD so only parent handles
            signal(SIGCHLD, SIG_IGN);

            //restore SIGTSTP and SIGINT to their original handlers
            signal(SIGTSTP, handlerStp);
            signal(SIGINT, handlerInt);

            //check for input/output redirection
            redirOut(args, tokenIndex, stdOut);
            redirInput(args, tokenIndex, stdIn);

            if (execvp(args[0], args) == -1)
            {
                perror("Failed to run");
                exit(EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        }
        else
        {
            int status;

            //if not in background, collect pid
            if (!background)
            {
                if (waitpid(pid_result, &status, WUNTRACED) == -1)
                {
                    perror("Error on wait");
                    exit(EXIT_FAILURE);
                }
            }

            if (WIFEXITED(status))
            {
                int exitS;
                char *succ;
                exitS = WEXITSTATUS(status);

                //assign EXITCODE
                if (exitS == 0)
                {
                    succ = "0";
                }
                else if (exitS == 1)
                {
                    succ = "1";
                }
                else if (exitS == 2)
                {
                    succ = "2";
                }
                else if (exitS == -1)
                {
                    succ = "-1";
                }

                setenv("EXITCODE", succ, 1);
            }
        }
    }
}

//Resolve special characters and check validity of arguments.
//If argument is found to be invalid, return false.
bool resolveArgs(char *args[], int tokenIndex){
    char temp[MAX_ARGS][1024];
    char fVal[1024];
    char v[1024];
    char *subs = NULL;
    int subsPos;
    char *slash = NULL;
    int slashPos;
    char tempChar;
    char *newLine = NULL;
    int newLinePos;

    //make a copy of arguments for modifications
    for(int i = 0;i<tokenIndex;i++){
        strcpy(temp[i], args[i]);
    }

    //check validity of input/output redirection 
    for (int i = 0; i < tokenIndex; i++)
    {
        if (args[i][0] == '>' || strcmp(args[i], ">>") == 0 || args[i][0] == '<')
        {
            if (args[i + 1] == NULL || i == 0)
            {
                printf("Missing Argument\n");
                return false;
                
            }
        }
    }

    //check validity of other special characters and act accordingly
    for (int i = 0; i < tokenIndex; ++i)
    {
        subs = memchr(temp[i], '$', strlen(temp[i]));

        //Loop as long as '$' is found
        while (subs)
        {
            subsPos = (int)(subs - temp[i]);

            //In case substitution is found in a long string argument,
            //cut off string where '$' starts. Else take entire argument for substitution.
            //After successful substitution, rejoin the string back together.
           if(temp[i][0] != '$' && (subs - 1)[0] != '\\')
            {
                tempChar = temp[i][subsPos - 1];
                temp[i][subsPos - 1] = '\0';

                if (SubsCheck(subs, fVal) == true)
                {
                    snprintf(v, MAXS, "%s%c%s", temp[i],tempChar, fVal);
                    strcpy(temp[i], v);
                }
                else
                {
                    return false;
                }
            }
            else if((subs - 1)[0] != '\\')
            {   
                tempChar = temp[i][subsPos - 1];               
                if (SubsCheck(subs, fVal) == true)
                {
                    snprintf(temp[i], MAXS, "%s", fVal);
                }
                else
                {
                    return false;
                }
            }
            subs = memchr(subs + 1, '$', strlen(subs + 1));
        }
        
        slash = memchr(temp[i], '\\', strlen(temp[i]));

        //Loop as long as '\' is found in front of special characters
        while (slash && (slash[1] == '"' || slash[1] == '$'))
        {
            slashPos = (int)(slash - temp[i]);
            int len = (int)strlen(temp[i]);

            if (temp[i][slashPos] == '\\')
            {
                for (int j = slashPos; j < len; ++j)
                {
                    temp[i][j] = temp[i][j + 1];
                }
            }
            slash = memchr(temp[i], '\\', strlen(temp[i]));
        }
        
        newLine = memrchr(temp[i], '\n', strlen(temp[i]));

        //If newline is found, remove it (file reading)
        if(newLine){
            newLinePos = (int)(newLine - temp[i]);

            temp[i][newLinePos] = '\0';
        }

        //assign arranged inputs to args
        args[i] = temp[i];
    }
    return true;
}

//Open files to receive input
bool redirOut(char *args[],int tokenIndex, int stdOut){
    int fD;
    stdOut = -1;
    for(int i = 0;i<tokenIndex;i++){
        if (strcmp(args[i],">") == 0 && args[i + 1] != NULL)
        {
            fD = open(args[i + 1], O_CREAT | O_TRUNC | O_WRONLY, 0600);
            if(fD == -1){
                perror("Could not find file");
                return false;
            }
            
            //store original fileno for restore later
            stdOut = dup(STDOUT_FILENO);

            //change output to filedescriptor
            dup2(fD, 1);
            close(fD);

            //set current position to NULL
            args[i] = NULL;

            return true;

        }else if(strcmp(args[i],">>") == 0 && args[i + 1] != NULL){

            fD = open(args[i + 1], O_CREAT | O_WRONLY | O_APPEND, 0600);
            if(fD == -1){
                perror("Could not find file");
                return false;
            }

            //store original fileno for restore later
            stdOut = dup(STDOUT_FILENO);
            
            //change output to filedescriptor
            dup2(fD, 1);
            close(fD);

            //set current position to NULL
            args[i] = NULL;

            return true;
        }
    }
    return false;
}

//Open files for reading
bool redirInput(char *args[], int tokenIndex, int stdIn){
    int fD;
    for(int i = 0;i<tokenIndex;i++){
        if(strcmp(args[i], "<") == 0 && args[i+1] != NULL){

            //Input validation
            if(strcmp(args[i+1], "|") != 0 && strcmp(args[i+1], ">") != 0 && strcmp(args[i+1], ">>")!= 0){

              fD = open(args[i+1], O_RDONLY, 0600);
                if(fD == -1){
                    perror("Could not open file");
                    return false;
                }

                //set current position to NULL
                args[i] = NULL;

                //Change input to filedescriptor
			    dup2(fD, STDIN_FILENO);
			    close(fD);

                return true;
            }
        }
    }
    return false;
}

//Restore original fileno for STDOUT
void restoreOut(int stdOut){
    dup2(stdOut, STDOUT_FILENO);
}

//If '&' is found, change position to NULL and return true.
bool runBackground(char *args[], int tokenIndex){
    
    if(strcmp(args[tokenIndex-1], "&") == 0){
        args[tokenIndex-1] = NULL;
        return true;
    }else return false;
}

//Handle piping
//If no pipes are present, return false, else return true.
//Piping is done by using two pipes and connecting the processes to each other based on if they are:
//first, last, even or odd positions. 
bool piping(char *args[], int tokenIndex){
    int totalCommands = 1;
    int numComm = 0;
    char *command[MAX_ARGS];
    int cnt = 0;
    int fd1[2];
    int fd2[2];
    pid_t pid;
    int stdOut;
    int stdIn;
    int curLen = 0;
    bool even = false;

    //Count number of processes to be piped
    for(int i = 0; i< tokenIndex;i++){
        if(strcmp(args[i], "|") == 0){
            totalCommands++;
        }
    }

    if(totalCommands > 1){

        //Loop until commands are done or current argument is NULL
        while(args[cnt] != NULL && numComm<totalCommands){
            //set flag for command position
            if(numComm%2 == 0){
                even = true;
            }
            else{
                even = false;
            }

            //Isolate current command to be executed
            curLen = 0;
            for(int i = cnt; i<tokenIndex;i++){
            
                if(strcmp(args[i], "|") == 0){   
                    command[i - cnt] = NULL;
                    cnt = i;
                    cnt++;
                    break;
                }
                
                command[i - cnt] = args[i];
                curLen++;
                
                if(i == tokenIndex-1){
                    command[i - cnt +1] = NULL;
                    cnt = i;
                    cnt++;
                }
            }

            //If command position is even, create pipe with fd1, else with fd2.
            //If last command, don't create a pipe
            if(even && numComm != (totalCommands-1)){
                pipe(fd1);
            }else if(numComm != (totalCommands-1)){
                pipe(fd2);
            }

            pid = fork();
            if(pid == -1){
                perror("fork failed");
                exit(EXIT_FAILURE);
            }

            //Child
            if(pid == 0){

                //check for input/output redirection
                if(!redirInput(command, curLen, stdIn)){
                redirOut(command, curLen, stdOut);
                }

                //If first command, connect write part of fd1 with output,
                //else if last command, check if even/odd position and 
                //connect the read ends respectively to input,
                //else if any command in between, check if even/odd position and
                //set respectively the read of one pipe to input and the write of 
                //the other pipe to output. Then execute process.
                if(numComm == 0){
                    close(fd1[0]);
                    dup2(fd1[1], STDOUT_FILENO);
                }else if(numComm == (totalCommands - 1)){
                    if(even){
                        dup2(fd2[0], STDIN_FILENO);
                    }else{
                        dup2(fd1[0], STDIN_FILENO);
                    }
                }else{
                    if(even){
                        close(fd1[0]);
                        dup2(fd2[0], STDIN_FILENO);
                        dup2(fd1[1], STDOUT_FILENO);
                    }else{
                        close(fd2[0]);
                        dup2(fd1[0], STDIN_FILENO);
                        dup2(fd2[1], STDOUT_FILENO);
                    }
                }

                if(execvp(command[0], command) == -1){
                    perror("Failed to run");
                    exit(EXIT_FAILURE);
                }

            }else{

                //Parent
                //Close the filedescriptors opened based on the same system
                //used to assign them.
                if (numComm == 0){
                    close(fd1[1]);
                }else if (numComm == (totalCommands - 1)){
                    if (even){
                        close(fd2[0]);
                    }else{
                        close(fd1[0]);
                    }
                }else{
                    if (even){
                        close(fd2[0]);
                        close(fd1[1]);
                    }else{
                        close(fd1[0]);
                        close(fd2[1]);
                    }
                }

                int status;
                waitpid(pid, &status, WUNTRACED);

                numComm++;
            }
        }

        return true;

    }else{
        return false;
    }

}